import 'package:flutter/material.dart';
import 'package:hello_flutter/models/person.dart';

class PersonCard extends StatefulWidget {
  final Person person;

  PersonCard({Key key, this.person}) : super(key: key);

  @override
  State<StatefulWidget> createState() => PersonCardState(person: person);
}

class PersonCardState extends State<PersonCard> {
  // Fields
  Person person;

  PersonCardState({this.person});

  @override
  Widget build(BuildContext context) {
    if(person == null) {
      return Container(
        child: Text("Oops, this person does not exist."),
      );
    }

    // Show the person card.
    return Card(
      child: InkWell(
        child: Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 17, left: 13, bottom: 4),
                    child: Text(person.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 13),
                    child: Text(person.role, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.grey)),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.favorite),
                    color: person.important ? Colors.red : Colors.grey,
                    onPressed: () {
                      setState(() {
                        person.important = !person.important;
                      });
                    },
                    tooltip: person.important ? "Remove from important people" : "Add to important people",
                  )
                ],
              )
            ],
          ),
        ),
        onTap: () {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text("You tapped ${person.name}."),
              action: SnackBarAction(
                label: "Okay",
                onPressed: () {},
              ),
            )
          );
        },
      ),
    );
  }
}
