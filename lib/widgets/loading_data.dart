import 'package:flutter/material.dart';

class LoadingData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        CircularProgressIndicator(),
        Padding(padding: EdgeInsets.all(10)),
        Text("Loading...")
      ],
    );
  }
}
