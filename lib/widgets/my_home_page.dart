import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hello_flutter/models/person.dart';
import 'package:hello_flutter/widgets/loading_data.dart';
import 'package:hello_flutter/widgets/person_card.dart';
import 'package:http/http.dart' as http;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  static final String dataUri = "https://gitlab.com/afzaal-ahmad-zeeshan/cloud-storage-flutter/raw/ef8f89b6cb4a2980ccce77e1b85ff42c925aaa36";
  var httpClient = http.Client();

  MyHomePageState() {
    getPeople();
  }

  bool error;
  String errorMessage;
  List<Person> people;

  void getPeople() async {
    try {
      var response = await httpClient.get("$dataUri/data.json");
      if(response.statusCode == 200 || response.statusCode == 304) {
        var downloadedPeople = List<Person>();
        var parsedResponse = jsonDecode(response.body) as List<dynamic>;
        for (dynamic object in parsedResponse) {
          downloadedPeople.add(
            Person(id: object["id"], name: object["name"], role: object["role"], age: object["age"], important: object["important"])
          );
        }
        setState(() {
          people = downloadedPeople;
        });
      } else {
        setState(() {
          error = true;
          errorMessage = "Perhaps the server is down.";
        });
      }
    } catch (e, _) {
      setState(() {
        error = true;
        errorMessage = e.toString();
      });
    }
  }

  Widget getWidget() {
    if(error != null && error) {
      return Text("Oops, cannot load the data. $errorMessage");
    } else if (people == null) {
      return LoadingData();
    } else {
      return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: people.length,
        itemBuilder: (buildcontext, index) {
          return PersonCard(person: people[index],);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: people == null ? MainAxisAlignment.center : MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            getWidget()
          ],
        ),
      ),
    );
  }
}
