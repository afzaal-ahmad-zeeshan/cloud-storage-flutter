import 'package:flutter/material.dart';
import 'package:hello_flutter/widgets/my_home_page.dart';

void main() => runApp(MyApp(homePage: MyHomePage(title: 'Cloud Storage for Flutter')));

class MyApp extends StatelessWidget {
  final Widget homePage;

  MyApp({@required this.homePage});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: homePage,
      debugShowCheckedModeBanner: false,
    );
  }
}
