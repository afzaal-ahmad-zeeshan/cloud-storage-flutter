class Person {
  int id;
  String name;
  String role;
  int age;
  bool important; 

  Person({this.id, this.name, this.role, this.age, this.important});
}
