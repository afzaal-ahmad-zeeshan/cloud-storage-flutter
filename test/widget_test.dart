import 'package:flutter_test/flutter_test.dart';
import 'package:hello_flutter/main.dart';
import 'package:hello_flutter/models/person.dart';
import 'package:hello_flutter/widgets/my_home_page.dart';
import 'package:hello_flutter/widgets/person_card.dart';

void main() {
  testWidgets('Loads properly', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp(homePage: MyHomePage(title: 'Cloud Storage for Flutter')));

    final loadingTextFinder = find.text("Loading...");
    expect(loadingTextFinder, findsOneWidget);
  });

  testWidgets('Show a proper error message', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp(homePage: PersonCard(person: null)));

    final loadingTextFinder = find.text("Oops, this person does not exist.");
    expect(loadingTextFinder, findsOneWidget);
  });

  testWidgets('Show the person details', (WidgetTester tester) async {
    await tester.pumpWidget(
                    MyApp(
                      homePage: PersonCard(
                        person: Person(id: 1, name: "Afzaal Ahmad Zeeshan", role: "Software Engineer", age: 24, important: true)
                      )
                    )
                  );

    final loadingTextFinder = find.text("Afzaal Ahmad Zeeshan");
    expect(loadingTextFinder, findsOneWidget);
  });
}
