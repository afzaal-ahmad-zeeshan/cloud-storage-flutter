## Cloud Storage for Flutter
A demo application that demonstrates how we can use cloud based storage and HTTP APIs to download and present the data to users using Flutter application. 

[<img src="screenshot.jpg" alt="Screenshot for the app on Android" width="250" />](/screenshot.jpg)  
_**Figure 1**: Default page as shown on Android platfrom._

I have used Alibaba Cloud storage in this application to demonstrate how we can download the content from a cloud storage, and render our `Widget`s. You can use other storage options too. 

App converts the incoming data to `Person` types:

``` dart
class Person {
  int id;
  String name;
  String role;
  int age;
  bool important; 

  Person({this.id, this.name, this.role, this.age, this.important});
}
```

Currently I am looking forward to creating a new constructor that creates an instance using `dynamic` parsing of `String` to `Object`. That will fix (currently existing) problem of creating instances outside the class definition:

``` dart
// In my_home_page.dart file
downloadedPeople.add(
    Person(id: object["id"], name: object["name"], role: object["role"], age: object["age"], important: object["important"])
);
```
Other than that, this project is functioning just fine. 

> You can download the built version of this app from [GitLab Pipelines](https://gitlab.com/afzaal-ahmad-zeeshan/cloud-storage-flutter/pipelines). Make sure you download and try out the latest version, only.
> Also note that I will change the Alibaba Cloud URL soon, and will enable a text field for you to enter your own custom Alibaba Cloud OSS data file.

### Object structure
Alibaba Cloud OSS service stores the content as the following:

``` json
[
    {
        "id": 1,
        "name": "Afzaal Ahmad Zeeshan",
        "role": "Software Engineer",
        "age": 25,
        "important": false
    },
    {
        "id": 2,
        "name": "Daniyal Ahmad Rizwan",
        "role": "Student",
        "age": 19,
        "important": true
    },
    {
        "id": 3,
        "name": "Bruce Wayne",
        "role": "The Dark Knight",
        "age": 80,
        "important": true
    },
    {
        "id": 4,
        "name": "Marshall Mathers",
        "role": "Motivation",
        "age": 47,
        "important": true
    }
]
```

This is then used to show the layout.

### Specs
Apart from the Flutter SDK, I also used `http` package. 

I have added a couple of tests to explain the Flutter testing framework. You can explore the **widget_test.dart** file to explore those. 

## Contribute
Feel free to submit an MR for any improvements/bugs that you found.